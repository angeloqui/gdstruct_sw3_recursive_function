#include <iostream>
#include <string>

using namespace std;

void printSum(int n, float sum) {

	float f = (float) n / 10;
	n = n / 10;
	f -= n;
	sum += (f * 10);

	if (n == 0) {
		cout << sum;
		return;
	}

	printSum(n, sum);
}

void printFibonacci(int n, int current, int previous) {
	
	n--;

	if (n < 0) {
		return;
	}

	cout << current << " ";

	int i = current;
	current = current + previous;
	previous = i;

	printFibonacci(n, current, previous);
}

void printPrime(int n, int i) {

	i++;
	
	if (n % i == 0) {
		cout << "NOT PRIME";
		return;
	}

	if (i == n / 2) {
		cout << "PRIME";
		return;
	}

	printPrime(n, i);
}

int main() {
	
	int option;

	while (true) {
		cout << "\n1. Compute the sum of digits of a number:";
		cout << "\n2. Print the fibonacci numbers up to n";
		cout << "\n3. Check a number whether it is prime or not";

		cout << string(2, '\n');
		cout << "input: ";
		cin >> option;

		int n;
		cout << string(2, '\n');
		cout << "input: ";
		cin >> n;

		if (option == 1) {

			float sum = 0;

			printSum(n, sum);
			cout << endl;
			system("pause");
		}
		else if (option == 2) {

			int previous = 1;
			int current = 0;

			printFibonacci(n, current, previous);
			cout << endl;
			system("pause");
		}
		else if (option == 3) {

			int i = 2;

			printPrime(n, i);
			cout << endl;
			system("pause");
		}

		system("cls");
	}
}